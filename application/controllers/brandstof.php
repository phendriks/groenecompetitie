<?php

class Brandstof_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('verbruik.brandstof.show');
	}

	public function action_data() {
		return Response::json(self::VerbruiksData());
	}

	public function VerbruiksData(){
		
			$temp["Benzine"] = array();
			$temp["Diesel"] = array();

			$color = 1;
			
				$temp["Benzine"]["label"] = "Benzine";
				$temp["Benzine"]["color"] = $color++;
				$temp["Benzine"]["data"] = array();

				$temp["Diesel"]["label"] = "Diesel";
				$temp["Diesel"]["color"] = $color++;
				$temp["Diesel"]["data"] = array();

				$query = DB::query('select Datum,Benzine,Diesel from Brandstof Order by Datum ASC');

				foreach ($query as $key => $value) {

					$datumstamp = (strtotime($value->datum)*1000);
					$tempBenzine[0] = $datumstamp;
					$tempBenzine[1] = (double)$value->benzine;

					$tempDiesel[0] = $datumstamp;
					$tempDiesel[1] = (double)$value->diesel;

					array_push($temp["Benzine"]["data"], $tempBenzine);
					array_push($temp["Diesel"]["data"], $tempDiesel);
				}

		return $temp;
	}
}