<?
class SoortenRitten_Controller extends Base_Controller{

	public function action_index(){
		return Response::eloquent(SoortenRitten::all()->get());
	}

	public function action_search($where, $value){
		return Response::eloquent(SoortenRitten::where($where, 'LIKE', "%".$value."%")->get());
	}

}