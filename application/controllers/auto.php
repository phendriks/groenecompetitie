<?php

class Auto_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('auto.show');
	}

	public function action_update(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$id = $input['id'];
		$kenteken = $input['kenteken'];
		$merk = $input['merk'];
		$model = $input['model'];
		$brandstof = $input['brandstof'];
		$datum = $input['datumaanschaf'];
		$inhoud = $input['tankinhoud'];
		$km = $input['kilometers'];
		$verbruik = $input['gemiddeldverbruik'];

		// Auto instantieren om een row te wijzigen.
		$auto = Auto::find($id);
		$auto->Kenteken = $kenteken;
		$auto->Merk = $merk;
		$auto->Model = $model;
		$auto->Brandstof = $brandstof;
		$auto->DatumAanschaf = $datum;
		$auto->TankInhoud = $inhoud;
		$auto->Kilometers = $km;
		$auto->GemiddeldVerbruik = $verbruik;
		$auto->save();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_add(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$kenteken = $input['kenteken'];
		$merk = $input['merk'];
		$model = $input['model'];
		$brandstof = $input['brandstof'];
		$datum = $input['datumaanschaf'];
		$inhoud = $input['tankinhoud'];
		$km = $input['kilometers'];
		$verbruik = $input['gemiddeldverbruik'];

		// Auto instantieren om een row aan te maken en in te vullen met de gegevens die door de post van jtable meegegeven zijn.
		$auto = new Auto();
		$auto->Kenteken = $kenteken;
		$auto->Merk = $merk;
		$auto->Model = $model;
		$auto->Brandstof = $brandstof;
		$auto->DatumAanschaf = $datum;
		$auto->TankInhoud = $inhoud;
		$auto->Kilometers = $km;
		$auto->GemiddeldVerbruik = $verbruik;
		$auto->save();
		// niet meer zeker of er een antwoord moet terug komen van jtable maar anders voor het geval dat een OK sturen.
		Return Response::json(array('Result' => 'OK', 'Record' => $input));
	}

	public function action_delete(){
		// afvangen input in variabele id
		$id = Input::get('id');
		// id waarop gezocht wordt
		$auto = Auto::find($id);
		$auto->delete();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_list(){
		
		$order = Input::get('jtSorting');
		$start = Input::get('jtStartIndex');
		$pagesize = Input::get('jtPageSize');
		$autos = array();
		
		$table['Result'] = 'ERROR';
		
		if($order != null && $start != null && $pagesize != null) {
			$autos = DB::query('select * from Auto ORDER BY '.$order.' LIMIT '.$start.','.$pagesize.'');
			$table['Result'] = 'OK';
			$table['TotalRecordCount'] = DB::table('Auto')->count();
			$table['Records'] = $autos;
		}
		else {
			$table['Message'] = "Er gaat iets niet helemaal goed in de aanvraag";
		}
		
		Return Response::json($table);
	}
	public function action_json(){
		$autos = Auto::all();
		Return Response::json($autos);
	}
}