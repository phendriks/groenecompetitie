<?php

class Chauffeurs_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('chauffeurs.show');
	}

	public function action_update(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$id = $input['id'];
		$naam = $input['naam'];
		$adres = $input['adres'];
		$postcode = $input['postcode'];
		$woonplaats = $input['woonplaats'];
		$leeftijd = $input['leeftijd'];
		$geslacht = $input['geslacht'];
		$afkorting = $input['afkorting'];

		// Chauffeur instantieren om een row te wijzigen.
		$chauffeur = Chauffeurs::find($id);
		$chauffeur->Naam = $naam;
		$chauffeur->Adres = $adres;
		$chauffeur->Postcode = $postcode;
		$chauffeur->Woonplaats = $woonplaats;
		$chauffeur->Leeftijd = $leeftijd;
		$chauffeur->Geslacht = $geslacht;
		$chauffeur->Afkorting = $afkorting;
		$chauffeur->save();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_add(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		//$id = $input['id'];
		$naam = $input['naam'];
		$adres = $input['adres'];
		$postcode = $input['postcode'];
		$woonplaats = $input['woonplaats'];
		$leeftijd = $input['leeftijd'];
		$geslacht = $input['geslacht'];
		$afkorting = $input['afkorting'];

		// chauffeur instantieren om een row te wijzigen.
		$chauffeur = new Chauffeurs();
		$chauffeur->Naam = $naam;
		$chauffeur->Adres = $adres;
		$chauffeur->Postcode = $postcode;
		$chauffeur->Woonplaats = $woonplaats;
		$chauffeur->Leeftijd = $leeftijd;
		$chauffeur->Geslacht = $geslacht;
		$chauffeur->Afkorting = $afkorting;
		$chauffeur->save();
		// niet meer zeker of er een antwoord moet terug komen van jtable maar anders voor het geval dat een OK sturen.
		Return Response::json(array('Result' => 'OK', 'Record' => $input));
	}

	public function action_delete(){
		// afvangen input in variabele id
		$id = Input::get('id');
		// id waarop gezocht wordt
		$chauffeur = Chauffeurs::find($id);
		$chauffeur->delete();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_list(){
		
		$order = Input::get('jtSorting');
		$start = Input::get('jtStartIndex');
		$pagesize = Input::get('jtPageSize');
		$chauffeurs = array();
		
		$table['Result'] = 'ERROR';
		
		if($order != null && $start != null && $pagesize != null) {
			$chauffeurs = DB::query('select * from Chauffeurs ORDER BY '.$order.' LIMIT '.$start.','.$pagesize.'');
			$table['Result'] = 'OK';
			$table['TotalRecordCount'] = DB::table('Chauffeurs')->count();
			$table['Records'] = $chauffeurs;
		}
		else {
			$table['Message'] = "Er gaat iets niet helemaal goed in de aanvraag";
		}
		
		Return Response::json($table);
	}
	public function action_json(){
		$chauffeurs = Chauffeurs::order_by('Naam')->get();
		Return Response::json($chauffeurs);
	}
}