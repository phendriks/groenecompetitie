<?php

class VerbruikPerChauffeur_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('verbruik.perchauffeur.show');
	}

	public function action_ritdata($Afkorting) {
		return Response::json(self::DataVanChauffeur($Afkorting));
	}

	public function DataVanChauffeur($chauffeur){
		$data = DB::query("select A.id, Kenteken From Auto as A JOIN Ritten as R ON A.Kenteken=R.Auto Where R.Chauffeur = '".$chauffeur."' order by Kenteken ASC");
			
			foreach ($data as $value) {
				$index = $value->kenteken;
				$temp[$index] = array();
				
				$temp[$index]["label"] = $value->kenteken;
				$temp[$index]["color"] = (int) $value->id;
				$temp[$index]["data"] = array();

				
				$data = DB::query("select Auto,strftime('%s', Datum) as Datum, ((Eindstand-Beginstand)/Liters) as KmpLiter From Ritten where Auto = '$value->kenteken' and Chauffeur = '".$chauffeur."' order by Datum ASC");
					foreach ($data as $key => $value) {
						
						$temp2[0] = (int)$value->datum * 1000;
						$temp2[1] = (double)$value->kmpliter;
						array_push($temp[$index]["data"], $temp2);
					}
				
			}

		return $temp;
	}
}