<?php

class Medewerker_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('medewerker.show');
	}

	public function action_update(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$id = $input['id'];
		$user = $input['username'];
		$pass = $input['password'];
		$email = $input['emailadres'];

		// Medewerker instantieren om een row te wijzigen.
		$medewerker = Medewerker::find($id);
		$medewerker->username = $user;
		$medewerker->password = Hash::make($pass);
		$medewerker->emailadres = $email;
		$medewerker->save();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_add(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$user = $input['username'];
		$pass = $input['password'];
		$email = $input['emailadres'];

		// Medewerker instantieren om een row te wijzigen.
		$medewerker = new Medewerker();
		$medewerker->username = $user;
		$medewerker->password = Hash::make($pass);
		$medewerker->emailadres = $email;
		$medewerker->save();
		// niet meer zeker of er een antwoord moet terug komen van jtable maar anders voor het geval dat een OK sturen.
		Return Response::json(array('Result' => 'OK', 'Record' => $input));
	}

	public function action_delete(){
		// afvangen input in variabele id
		$id = Input::get('id');
		// id waarop gezocht wordt
		$medewerker = Medewerker::find($id);
		$medewerker->delete();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_list(){
		
		$order = Input::get('jtSorting');
		$start = Input::get('jtStartIndex');
		$pagesize = Input::get('jtPageSize');
		$medewerkers = array();
		
		$table['Result'] = 'ERROR';
		
		if($order != null && $start != null && $pagesize != null) {
			$medewerkers = DB::query('select * from users ORDER BY '.$order.' LIMIT '.$start.','.$pagesize.'');
			$table['Result'] = 'OK';
			$table['TotalRecordCount'] = DB::table('users')->count();
			$table['Records'] = $medewerkers;
		}
		else {
			$table['Message'] = "Er gaat iets niet helemaal goed in de aanvraag";
		}
		
		Return Response::json($table);
	}
}