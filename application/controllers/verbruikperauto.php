<?php

class VerbruikPerAuto_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('verbruik.perauto.show');
	}

	public function action_ritdata($kenteken) {
		return Response::json(self::DataVanAuto($kenteken));
	}

	public function DataVanAuto($kenteken){
		$data = DB::query("select C.id, C.Naam, C.Afkorting From Chauffeurs as C JOIN Ritten as R ON C.Afkorting=R.Chauffeur Where R.Auto = '".$kenteken."' order by R.Chauffeur ASC");
			
			foreach ($data as $value) {
				$index = $value->naam;
				$temp[$index] = array();
				
				$temp[$index]["label"] = $value->naam;
				$temp[$index]["color"] = (int) $value->id;
				$temp[$index]["data"] = array();

				
				$data = DB::query("select strftime('%s', Datum) as Datum, ((Eindstand-Beginstand)/Liters) as KmpLiter From Ritten where Chauffeur = '$value->afkorting' And Auto = '".$kenteken."' order by Datum ASC");
					foreach ($data as $key => $value) {
						
						$temp2[0] = (int)$value->datum * 1000;
						$temp2[1] = (double)$value->kmpliter;
						array_push($temp[$index]["data"], $temp2);
					}
				
			}

		return $temp;
	}
	public function action_gemiddelde($kenteken){
		$data = DB::query('select GemiddeldVerbruik from Auto where Kenteken = "'.$kenteken.'"');
		$mark = array();

		foreach ($data as $key => $value) {
			
			$tempMark["color"] = "red";
			$tempMark["lineWidth"] = 1;
			$tempMark["yaxis"]["from"] = (double)$value->gemiddeldverbruik;
			$tempMark["yaxis"]["to"] = (double)$value->gemiddeldverbruik;
			
			array_push($mark, $tempMark);

			
		}
		return Response::json($mark);
	}
}