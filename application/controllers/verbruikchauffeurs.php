<?php

class VerbruikChauffeurs_Controller extends Base_Controller{
	var $ReturnArrayData = array();
	var $DataVanChauffeur = array();
	var $NamenChauffeurs = array();
	var $ZoekChauffeurDetails = array();
	public function action_index(){
		Return View::make('verbruik.chauffeurs.show');
	}

	public function action_ritdata() {
		return Response::json(self::DataVanChauffeur());
	}

	public function DataVanChauffeur(){
		$data = DB::query("select c.id, Afkorting, Naam From Chauffeurs as C JOIN Ritten as R ON C.Afkorting=R.Chauffeur order by Naam");
		
			$color = 1;
			foreach ($data as $key => $value) {
				$temp[$value->afkorting] = array();
				
				$temp[$value->afkorting]["label"] = $value->naam;
				$temp[$value->afkorting]["color"] = (int)$value->id;
				$temp[$value->afkorting]["data"] = array();

				
				$data = DB::query("select Chauffeur,strftime('%s', Datum) as Datum, ((Eindstand-Beginstand)/Liters) as KmpLiter From Ritten where Chauffeur = '$value->afkorting' order by Datum ASC");
					foreach ($data as $key => $value) {
						
						$temp2[0] = (int)$value->datum * 1000;
						$temp2[1] = (double)$value->kmpliter;
						array_push($temp[$value->chauffeur]["data"], $temp2);
					}
				
			}

		return $temp;
	}
}