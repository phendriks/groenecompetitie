<?php

class Ritten_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('ritten.show');
	}
	public function action_update(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$id = $input['id'];
		$datum = $input['datum'];
		$auto = $input['auto'];
		$chauffeur = $input['chauffeur'];
		$beginstand = $input['beginstand'];
		$eindstand = $input['eindstand'];
		$liters = $input['liters'];

		// Rit instantieren om een row toe te voegen.
		$ritten = Ritten::find($id);
		$ritten->Datum = $datum;
		$ritten->Auto = $auto;
		$ritten->Chauffeur = $chauffeur;
		$ritten->Beginstand = $beginstand;
		$ritten->Eindstand = $eindstand;
		$ritten->Liters = $liters;
		$ritten->save();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_add(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$datum = $input['datum'];
		$kenteken = $input['auto'];
		$chauffeur = $input['chauffeur'];
		(double)$beginstand = $input['beginstand'];
		(double)$eindstand = $input['eindstand'];
		$liters = $input['liters'];

		$auto = Auto::where('Kenteken', '=', $kenteken)->first();
		$kilometerstand = (double)$auto->kilometers;
		if($kilometerstand < $beginstand)
		{
		// Rit instantieren om een row toe te voegen.
		$autoUpdate = Auto::where('Kenteken', '=', $kenteken)->first();
		$autoUpdate->Kilometers = (string)$eindstand;
		$autoUpdate->save();
		$ritten = new Ritten();
		$ritten->datum = $datum;
		$ritten->auto = $kenteken;
		$ritten->chauffeur = $chauffeur;
		$ritten->beginstand = $beginstand;
		$ritten->eindstand = $eindstand;
		$ritten->liters = $liters;
		$ritten->save();
		//niet meer zeker of er een antwoord moet terug komen van jtable maar anders voor het geval dat een OK sturen.
		Return Response::json(array('Result' => 'OK', 'Record' => $input));
		}
		else {
			Return Response::json(array('Result' => 'ERROR', 'Message' => "FOUT: De beginstand is lager dan wat er in de Database aan kilometers geregisteerd staan : $kilometerstand"));
		}
	}

	public function action_delete(){
		// afvangen input in variabele id
		$id = Input::get('id');
		// id waarop gezocht wordt
		$ritten = Ritten::find($id);
		$ritten->delete();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_list(){
		
		$order = Input::get('jtSorting');
		$start = Input::get('jtStartIndex');
		$pagesize = Input::get('jtPageSize');
		$Ritten = array();
		
		$table['Result'] = 'ERROR';
		
		if($order != null && $start != null && $pagesize != null) {
			$Ritten = DB::query('select * from Ritten ORDER BY '.$order.' LIMIT '.$start.','.$pagesize.'');
			$table['Result'] = 'OK';
			$table['TotalRecordCount'] = DB::table('Ritten')->count();
			$table['Records'] = $Ritten;
		}
		else {
			$table['Message'] = "Er gaat iets niet helemaal goed in de aanvraag";
		}
		
		Return Response::json($table);
	}

	public function action_chauffeurs(){
		$options['Result'] = "OK";
		$options['Options'] = array();
		$result =  DB::query('select Naam,Afkorting from Chauffeurs ORDER BY Naam ASC');

		foreach ($result as $key => $value) {
			$tmpArray['DisplayText'] = $value->naam;
			$tmpArray['Value'] = $value->afkorting;
			array_push($options['Options'], $tmpArray);
		}
		return Response::json($options);
	}

	public function action_autos(){
		$options['Result'] = "OK";
		$options['Options'] = array();
		$result =  DB::query('select Kenteken from Auto ORDER BY Kenteken ASC');

		foreach ($result as $key => $value) {
			$tmpArray['DisplayText'] = $value->kenteken;
			$tmpArray['Value'] = $value->kenteken;
			array_push($options['Options'], $tmpArray);
		}
		return Response::json($options);
	}
}