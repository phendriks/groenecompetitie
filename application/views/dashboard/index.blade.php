<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Dashboard - Groene Competitie - Van Delden Limousines</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}

{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js') }}

<script type="text/javascript">

    $(document).ready(function() {        
    
    $.get('http://localhost/prijzen.php', function(data) {
  $('#prijs').html("<table><th>Euro95&nbsp;</th><th>Diesel</th><tr><td class='groen'>&euro;" + data.benzine + "</td><td class='rood'>&euro;"+ data.diesel +"</td></tr></table>");
});
});

</script>

</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>
<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="{{ URL::to('/') }}">Van Delden</a></h1>
    <!-- nav -->
    <nav>
<ul id="nav">
  <li class="current"><a href="{{ URL::to('/') }}">Dashboard</a></li>
  <li><a href="#">Verbruik</a>
  <ul>
    <li><a href="{{ URL::to('verbruikchauffeurs') }}">Chauffeurs</a></li>
    <li><a href="{{ URL::to('verbruikauto') }}">Auto's</a></li>
    <li><a href="{{ URL::to('verbruikperchauffeur') }}">Chauffeur</a></li>
    <li><a href="{{ URL::to('verbruikperauto') }}">Auto</a></li>
    <li><a href="{{ URL::to('ritten') }}">Ritten Invoeren</a></li>
    <li><a href="{{ URL::to('brandstof') }}">Brandstof Overzicht</a></li>
  </ul>
  <li><a href="{{ URL::to('chauffeurs') }}">Chauffeurs</a></li>
  <li><a href="{{ URL::to('medewerker') }}">Medewerkers</a></li>
  <li><a href="{{ URL::to('auto') }}">Auto's</a>
  
    </li>
  <li><a href="{{ URL::to('uitloggen') }}">Uitloggen</a></li>
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  <div id="prijs"></div>
  <h2 class="ribbon">Welkom {{ Auth::user()->username }}!</h2>

  <div class="triangle-ribbon"></div>
  <div id="chart" style="width:850px; height:300px; margin:95px auto 25px auto;"></div>



  </div>
    
  <footer>
    <ul class="footer-nav">
      <li><a href="{{ URL::to('/') }}">Dashboard</a> |</li>
      <li><a href="http://www.vandeldenlimousines.nl">Van Delden Limousines</a></li>
    </ul>
    <p>Copyright &copy;2013, <a href="http://www.patrickpc.nl">Patrick Hendriks</a></p>
    <br class="cl" />
  </footer>
  
   
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>