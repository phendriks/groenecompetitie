<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Verbruik Auto - Groene Competitie - Van Delden Limousines</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}
{{ HTML::style('js/themes/metro/jtable_metro_base.min.css') }}
{{ HTML::style('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css') }}
{{ HTML::style('js/themes/metro/green/jtable.min.css') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js') }}
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="{{ URL::to('/') }}">Van Delden</a></h1>
    <!-- nav -->
    <nav>
<ul id="nav">
  
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <h2 class="ribbon">Inloggen</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
  
  {{ Form::open('verwerklogin') }}

    <!-- check for login errors flash var -->
    @if (Session::has('login_errors'))
        <span class="error">Gebruikersnaam en of wachtwoord is onjuist.</span>
    @endif

    <!-- username field -->
    <p>{{ Form::label('username', 'Username') }}</p>
    <p>{{ Form::text('username') }}</p>

    <!-- password field -->
    <p>{{ Form::label('password', 'Password') }}</p>
    <p>{{ Form::password('password') }}</p>

    <!-- submit button -->
    <p style="position:relative;left:140px;">{{ Form::submit('Login') }}</p>

{{ Form::close() }}
    
  </div>
  </div>
    
  
  
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>