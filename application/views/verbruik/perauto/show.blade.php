<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Verbruik Per Auto - Groene Competitie - Van Delden Limousines</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}
{{ HTML::style('js/themes/metro/jtable_metro_base.min.css') }}
{{ HTML::style('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css') }}
{{ HTML::style('js/themes/metro/green/jtable.min.css') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js') }}
{{ HTML::script('js/jquery.jtable.min.js') }}
{{ HTML::script('js/localization/jquery.jtable.nl-NL.js') }}
{{ HTML::script('js/flot/jquery.flot.js') }}
{{ HTML::script('js/flot/jquery.flot.time.js') }}
{{ HTML::script('js/flot/jquery.flot.selection.js') }}
<script type="text/javascript">   

$(document).ready(function () {
    
    var options;
    var autourl = "{{ URL::to('/auto/json') }}";
    var baseurl = "{{ URL::to('/verbruikperauto/') }}";
    var dataurl = baseurl + "ritdata";
    var datasets = [];
    var data = [];
    var plot;
    var scheidingslijn;


    var placeholder = $("#verbruikautos");
    var autoselect = $("#auto_opties");
    var choiceContainer = $("#choices");
    
        function Checkboxxes(data){
          
          $.each(data, function(key, val) {
          choiceContainer.append("<br/><input type='checkbox' name='" + key +
          "' checked='checked' id='id" + key + "'></input>" +
          "<label for='id" + key + "'>"
          + val.label + "</label>");
          });
        }
        function onDataReceivedAutos(autos)
        {
          $.each(autos, function(key, val) {
          autoselect.append("<option value='"+val.original.kenteken+"'>"+val.original.kenteken+"</option>");
          });
        }
        function onDataReceived(series) {
            datasets = series;
            Checkboxxes(series);
            choiceContainer.find("input").click(plotAccordingToChoices);
            data = plotAccordingToChoices();        
            placeholder.bind("plotselected", ZoomSelectie);

            plot = $.plot(placeholder, data, options);

            $("#clearselectie").click(function () {
              plot.clearSelection();
              $.plot(placeholder, data, options);
            });  
         }
         function OptionsAfterLoad(marks){
              scheidingslijn = marks;
              options = {
              lines: { show: true },
              points: { show: true },
              grid: { hoverable: true, clickable: true, markings: scheidingslijn },
              xaxis: { mode: "time", timeformat: "%d/%m/%Y", minTickSize: [1, "day"] },
              selection: { mode: "x" }
          };
         }
           
         autoselect.on('change', function() {
            data = null;
            datasets.length = 0;
            choiceContainer.html('');

            $.ajax({
            url: baseurl + "gemiddelde/"+this.value,
            method: 'GET',
            dataType: 'json',
            success: OptionsAfterLoad,
            error: RequestError
             }); 

            $.ajax({
            url: baseurl +"ritdata/" + this.value,
            method: 'GET',
            dataType: 'json',
            success: onDataReceived,
            error: RequestError
        });}); 

        $.ajax({
            url: autourl,
            method: 'GET',
            dataType: 'json',
            success: onDataReceivedAutos
        });
          function RequestError(){
            alert('Het lijkt erop dat er nog geen Data beschikbaar is om te tonen.');
          }  
          function ZoomSelectie(event, ranges) {
            var data = plotAccordingToChoices();
          if($("#zoom").is(':checked')){
            plot = $.plot(placeholder, data, $.extend(true, {}, options, {
              xaxis: {
                  min: ranges.xaxis.from,
                  max: ranges.xaxis.to
                }
              }));
            }
          }
      
        function plotAccordingToChoices() {
          var data = [];

          choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && datasets[key]) {
              data.push(datasets[key]);
            }
          });

          if (data.length > 0) {
            $.plot(placeholder, data, options);
          }
          return data;
        }
function showTooltip(x, y, contents) {
      $("<div id='tooltip'>" + contents + "</div>").css({
        position: "absolute",
        display: "none",
        top: y + 15,
        left: x + 15,
        border: "1px solid #fdd",
        padding: "2px",
        "background-color": "#fee",
        opacity: 0.80,
        "z-index": 999
      }).appendTo("body").fadeIn(200);
    }
    var previousPoint = null;
    placeholder.bind("plothover", function (event, pos, item) {
        if (item) {
          if (previousPoint != item.dataIndex) {

            previousPoint = item.dataIndex;

            $("#tooltip").remove();
            var x = item.datapoint[0],
            y = item.datapoint[1].toFixed(3);
            var date = new Date(x).toLocaleDateString();

            showTooltip(item.pageX, item.pageY,
                "<b>" + item.series.label + "</b> (" + date + ") [<b>" + y + "</b> Km/L]");
          }
        } else {
          $("#tooltip").remove();
          previousPoint = null;            
        }
      
    });
        

    });
</script>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="{{ URL::to('/') }}">Van Delden</a></h1>
    <!-- nav -->
    <nav>
<ul id="nav">
  <li><a href="{{ URL::to('/') }}">Dashboard</a></li>
  <li class="current"><a href="#">Verbruik</a>
  <ul>
    <li><a href="{{ URL::to('verbruikchauffeurs') }}">Chauffeurs</a></li>
    <li><a href="{{ URL::to('verbruikauto') }}">Auto's</a></li>
    <li><a href="{{ URL::to('verbruikperchauffeur') }}">Chauffeur</a></li>
    <li><a href="{{ URL::to('verbruikperauto') }}">Auto</a></li>
    <li><a href="{{ URL::to('ritten') }}">Ritten Invoeren</a></li>
    <li><a href="{{ URL::to('brandstof') }}">Brandstof Overzicht</a></li>
  </ul>
  <li><a href="{{ URL::to('chauffeurs') }}">Chauffeurs</a></li>
  <li><a href="{{ URL::to('medewerker') }}">Medewerkers</a></li>
  <li><a href="{{ URL::to('auto') }}">Auto's</a>
  
    </li>
  <li><a href="{{ URL::to('uitloggen') }}">Uitloggen</a></li>
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <h2 class="ribbon">Verbruik Per Auto</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
    <select id="auto_opties" style="float:left">
    </select>
    <br /><br />
    <div id="verbruikautos" style="float:left; width:750px; height:300px;"></div>
    <div id="choices" style="float:right; width:135px;"></div>
    <p style="float:right; width:135px;"><label><input id="zoom" type="checkbox"></input>Zoom</label></p>
    <p style="float:right; width:135px;"><label><a href="#" id="clearselectie" >Zoom uit</a></label></p>
  </div>
  </div>
    
  
  
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>