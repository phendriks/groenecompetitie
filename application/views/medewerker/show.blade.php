<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Medewerkers - Groene Competitie - Van Delden Limousines</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}
{{ HTML::style('js/themes/metro/jtable_metro_base.min.css') }}
{{ HTML::style('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css') }}
{{ HTML::style('js/themes/metro/green/jtable.min.css') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js') }}
{{ HTML::script('js/jquery.jtable.min.js') }}
{{ HTML::script('js/localization/jquery.jtable.nl-NL.js') }}
<script type="text/javascript">
    $(document).ready(function () {
        $('#medewerkerstablecontainer').jtable({
            title: 'Medewerkers',
            paging: true, 
            pageSize: 10, 
            sorting: true,
            ajaxSettings: { type: 'POST' },
            defaultSorting: 'username ASC',

            actions: {
                listAction: '{{ URL::to("/medewerker/list") }}',
                createAction: '{{ URL::to("/medewerker/add") }}',
                updateAction: '{{ URL::to("/medewerker/update") }}',
                deleteAction: '{{ URL::to("/medewerker/delete") }}'
            },
            fields: {
                id: {
                    key: true,
                    list: false,
                    create: false,
                    edit: false
                },
                username: {
                    title: 'Gebruikersnaam',
                    width: '10%'
                },
                password: {
                    title: 'Wachtwoord',
                    width: '5%',
                    type: 'password',
                    list: false,
                    edit: true,
                    create: true
                },
                emailadres: {
                    title: 'Emailadres',
                    width: '10%'
                },
                created_at: {
                    list: false,
                    edit: false,
                    create: false
                },
                updated_at: {
                    list: false,
                    edit: false,
                    create: false
                }
            }
        });
    $('#medewerkerstablecontainer').jtable('load');
    });

</script>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="{{ URL::to('/') }}">Van Delden</a></h1>
    <!-- nav -->
    <nav>
<ul id="nav">
  <li><a href="{{ URL::to('/') }}">Dashboard</a></li>
  <li><a href="#">Verbruik</a>
  <ul>
    <li><a href="{{ URL::to('verbruikchauffeurs') }}">Chauffeurs</a></li>
    <li><a href="{{ URL::to('verbruikauto') }}">Auto's</a></li>
    <li><a href="{{ URL::to('verbruikperchauffeur') }}">Chauffeur</a></li>
    <li><a href="{{ URL::to('verbruikperauto') }}">Auto</a></li>
    <li><a href="{{ URL::to('ritten') }}">Ritten Invoeren</a></li>
    <li><a href="{{ URL::to('brandstof') }}">Brandstof Overzicht</a></li>
  </ul>
  <li><a href="{{ URL::to('chauffeurs') }}">Chauffeurs</a></li>
  <li class="current"><a href="{{ URL::to('medewerker') }}">Medewerkers</a></li>
  <li><a href="{{ URL::to('auto') }}">Auto's</a>
  
    </li>
  <li><a href="{{ URL::to('uitloggen') }}">Uitloggen</a></li>
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <h2 class="ribbon">Medewerkers</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
  
    <div id="medewerkerstablecontainer"></div>

  </div>
  </div>
    
  
  
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>