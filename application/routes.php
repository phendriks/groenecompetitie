<?php

Route::group(array('before'=> 'auth'), function()
{
		Route::controller('auto'); 
		Route::controller('chauffeurs');
		Route::controller('dashboard');
		Route::controller('medewerker');
		Route::controller('ritten');
		Route::controller('verbruikchauffeurs');
		Route::controller('verbruikauto');
		Route::controller('brandstof');
		Route::controller('verbruikperauto');
		Route::controller('verbruikperchauffeur');
});
Route::filter('auth', function()
{
    if (Auth::guest()) return Redirect::to('login');
});
////////////// Login routes
Route::get('uitloggen', function() {
    Auth::logout();
    return Redirect::to('login');
});
Route::get('login', function()
{
return View::make('login.inloggen');
});
Route::post('verwerklogin', function(){
    $userdata = array(
        'username'      => Input::get('username'),
        'password'      => Input::get('password')
    );
    if ( Auth::attempt($userdata) )
    {
	return Redirect::to('dashboard');
	}
	else
    {
        return View::make('login.inloggen')
            ->with('login_errors', true);
    }
});

Route::get('/', array('before' => 'auth', 'do' => function() {
    return View::make('dashboard.index');
}));



Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function($exception)
{
	return Response::error('500');
});

//////////////
//////////////

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});